//
//  ContentView.swift
//  LandmarkSwiftUI
//
//  Created by Htet Myat Moe Myint Kyeal on 19/10/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        LandmarkList()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(ModelData())
    }
}
