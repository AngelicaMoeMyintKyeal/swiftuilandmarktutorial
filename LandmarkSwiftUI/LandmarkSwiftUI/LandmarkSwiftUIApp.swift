//
//  LandmarkSwiftUIApp.swift
//  LandmarkSwiftUI
//
//  Created by Htet Myat Moe Myint Kyeal on 19/10/22.
//

import SwiftUI

@main
struct LandmarksApp: App {
    @StateObject private var modelData = ModelData()

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(modelData)
        }
    }
}
